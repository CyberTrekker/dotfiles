# Mabox Linux

**What is Mabox Linux?**

Mabox provides a lightweight, fast and ready to use Linux system, of which is suitable even for but not exclusive to older and not so powerful computers.

Because it's based on the solid and stable foundations of Manjaro Linux's stable branch, it's not only a stable system but offers quick and easy access to the latest software versions available from the Manjaro and AUR repositories.

Mabox is rolling-release distribution.

Mabox derived its name from being based on Manjaro Linux and having Openbox as its default window manager. Mabox, straight out-of-the-box, so to speak, doesn't have a normal desktop environment installed but relies entirely on the Openbox WM with pre-configured components, which are highly configurable by the end-user, and is wonderfully functional.

**The Openbox WM Inclusion and Functionality Offers:**

    tint21 as the panel.

    The menus and sidepanels are based on jgmenu2.

    Several home-brewed tools and utilities are included.

    There are a few components comes from LXDE and Xfce.

    Along with the ones listed above, there are a few BunsenLabs3 tools
    adapted for Mabox (theme-manager, tint2 and conky manager scripts).
