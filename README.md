# CT's Dotfiles

**What is i3wm?**

Firstly and succinctly, i3 is a tiling/dynamic window manager (hence the wm in its name that's often used in reference to it) designed for X11. It was inspired by wmii and is written in C language. i3 supports tiling, stacking and tabbing layouts. These are handled dynamically. Configuration of i3 is done by using a plain text file named config. Extending i3 is achieved by using its Unix domain socket and JSON-based and IPC interface in many programming languages.

For those not in the know or of whom don't quite understand, a window manager controls and appearance of windows within a computer  or other relevant device windowing system utilizing a Graphical User Interface (GUI).

The i3wm is primarily targeted to power users, developers, and Linux hackers. Having said this, there's nothing to stop ordinary users from trying it out and if they like it, along with feeling comfortable with the idea of continuing using it, well and good for them.

**Features**

- Easy configuration using a plain text file, which can be executed at runtime for restarting services.
- Documentation from various sources.
- Uses a tree as the abstraction, and underlying data structure, for window management.
- The advantage of an implimentation of different modes (similar to the text editors vi and its clone vim). This permits keys to have different functions, depending on the mode the window manager is currently in.
- Uses asynchronous XCB library, instead of the old synchronous XLIB.

Although this point could've been included in the features section, I decided to give it a paragraph prominance of its own. The i3 window manager is above all free of the bloat so well known to conventional desktop environments. Therefore, it is an exceptional light-weight alternative for either those of whom have a dislike for bloat or for those with low-end systems. Don't let this fool you, though, as it can even be very handy in high-end systems too. Following the theme of being light-weight and minimalistic, i3wm is mostly keyboard-centric.

---
 
_i3wm Configuration Files_

**What Dotfiles Are, Further Explained**

To expand a little upon my introductory blurb and to make it more clear, a dotfile is generally any file that begins with a dot or full stop in Unix-like operating systems. Their purpose is to contain various settings and configurations. Traditionally, user-specific application configurations or, in other words, preferences, are stored dotfiles. 

The dotfiles contained in this repository, rather than being solely for my purpose, can be used as a type of template by others to configure their own dotfiles for their particular Linux systems.

**What's in this Repository?**

Basically, this repository contains my config files and scripts associated with my i3wm installs on several laptops - a couple now using the Manjaro Linux distribution and two others using the Fedora Linux distribution. It is my attempt at ricing (theming) and modifying my i3wm and i3-gaps installations.

**My Customized Window Manager Configs**

- i3wm
- i3-gaps

_i3 differentiation:_ When I mention i3 differentiation I am specifically referring to there being, for the purpose of considering the original from the fork, two major branches of the i3 window manager. Gaps is the namesake feature of the i3-gaps version of i3wm, which provides the functionality of adding spacing between windows/containers. There is other differential functionality in i3-gaps too - for instance, smart gaps, smart borders, smart edge borders, and i3bar height. Gaps comes in ostensibly two sorts, with these being inner and outer gaps. Inner gaps are those between two adjacent containers or a container and an edge.While outer gaps are an additional spacing along the screen edges.

My initial install and usage of the i3-gaps version of the i3wm was the furhmann/i3-gaps Copr repository verion for Fedora Linux on one of my laptops, then recently through an installation of the Manjaro Linux i3 edition from the Manjaro Community on another of my laptops with their already existing and basically configured to their specific release i3-gaps. The Fedora machines have now had the i3wm replaced with their i3-gaps specific version.

**_Note:_** When wm is used in the context of i3, it merely refers to the description "window manager". It, therefore, doesn't reference a different version of i3.

**Related Conf Files to My i3wm Customization in this Repository**

- i3status.conf
- conky.conf
- compton.conf
- picom.conf
- redshift.conf

**i3status** is a small program for generating a status bar for i3bar in i3wm. In other words, it's a lightweight status generator. i3status assists in displaying useful information on the i3bar. Configuration is effected through the i3status configuration file (i3status.conf), which is configurable by the user to add pertinent information or appearance for their particular usage.

**conky** is a free, lightweight system monitor for the x window system environment, with the capability to display several types of information on the user's desktop or even on the worksplaces of window managers. For example, it can display information about CPUs, memory, swap, disk space, temprature, top, upload and download.

**picom** is a standalone and lightweight Xorg compositor. It's especially useful for window managers (WMs) of which do not provide built-in compositing features themselves. Picom was previously a fork of another compositor called compton, which itself was a fork of xcompmgr. With a compositor like picom, the window manager user is able to configure in the picom configuration file (picom.conf) such useful features as transparency/opacity, blur, shadows, fading, etc. The i3 window manager doesn't come with compositor support built-in, so that's the reason for the need of compositors like picom.

**redshift** is a handy and very useful for adjusting the colour temperature of a user's screen according to their surroundings and time of the day. This has the convenient effect of reducing the screen glare, thereby contributing to the negation of negative effects of screen glare for the computer user (especially in dark environments). As with the above, redshift can be configured to the user's preference through the redshift configuration file (redshift.conf). A handy GUI for redshift is the redshift-gui tray icon, which permits the user to enable/disable redshift through it. The tray icon also offers the capability of suspending redshift for 30 minutes, 1 hour, or 2 hours.
