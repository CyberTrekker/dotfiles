**Manjaro Linux & i3-gaps on MacBook Air**
----
This folder contains, of course, the relevant configuration files for my MacBook Air Manjaro Linux i3 Community Edition installation.

There would be those, no doubt, of whom would wonder at and question why I would install a Linux distribution on an Apple device such as a MacBook Air that already has MacOS pre-installed. The Apple purists in particular would be agog and shaking their heads in disgust. However, due to Apple's business model and the changes in technology since this MacBook Air model was released, I find my MacBook Air being incapable of updating to the latest MacOS. My MacBook Air model is the mid 2011 release. Consequently, the last installable version of MacOS had become end-of-life (EOL) some time ago. Given this, it could no longer receive security or functional updates.

It was from this scenario that I found myself deciding to install Linux on it and, because of this, would be able to have security and feature updates through having a dedicated Linux installation machine. Hence, from my recent try out of the Manjaro Linux distribution on a couple of my other laptops, I chose the Manjaro i3 Community Edition for my MacBook Air over the equivalent of the Fedora Linux distribution.
