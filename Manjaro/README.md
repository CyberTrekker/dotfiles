**Manjaro Linux and the i3 Window Manager**
---

Manjaro is an open-source GNU/Linux operating system that's based on Arch Linux. It's a rolling-release Linux distibution and uses Pacman as its packet manager.

I use the Manjaro i3 Community Edition. This edition of the Manjaro Linux operating system is based around the i3-gaps fork of the i3 window manager.

My initial and first installation of Manjaro Linux, i3 Community Edition was and remains on my Acer Aspire 5740G laptop.

As you may have noticed, I have over time added to the Manjaro section of the repository in changing my mid 2011 MacBook Air from MacOS to Manjaro Linux with the i3 Community Edition (i3-gaps) as a sole and permanent replacement. 
