I had changed from the ordinary i3wm to i3-gaps version. However, the good news is gaps has now been merged into the normal i3wm.

The announcement on Reddit by the creator of the i3-gaps variant of i3wm:

PSA: i3-gaps has been merged into i3
Today is a monumental day for me -- by far the most commonly asked questions in my ~8 years of maintaining i3-gaps has been: "Why don't you merge it into i3?". Well, today is the day this question can finally come to an end.

What's happening?
As of today, we have merged all i3-gaps features into i3 (pull request). Except for minor technical differences, users will be able to switch to i3 with their i3-gaps config seamlessly.

Note that we decided to merge i3-gaps more or less "as-is". This means all the existing restrictions and issues will remain for the time being, but the major benefit here is that we got to just finally do it (after many years).

If you have any questions, I'll do my best to answer them.

What's next?
For now, the change has been merged, but not yet released (it will be i3 4.22). As some (but not many) changes were made, it would be appreciated if people could give the current i3 HEAD a try to make sure things are working as before.

What does this mean for i3-gaps?
I will archive the project on GitHub and redirect to i3 instead. The repository will remain, however. Any issues worth keeping will be migrated to i3.

Once i3 4.22 is released, distros can simply replace any i3-gaps package with the i3 one, and everything should happen automatically for users.

What does this mean for me?
For me this is the end of an almost decade-long journey with i3-gaps. It started in r/unixporn, and it led me to become a maintainer for i3. i3 and i3-gaps have had a big impact on my life, private and professional, good and bad. So a "thank you" to everyone along the way, from Michael, TonyC and Orestis (maintainers) to all the people in the community and just everyone who enjoys using i3.
