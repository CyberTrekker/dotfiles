**The Introduction of an i3 Spin to the Fedora Linux Offerings**
---
Since the release of Fedora 34 an officially created i3wm spin of the Fedora Linux operating system has been released and mad available on the Fedora website. The i3wm spin was initiated as a project by those comprising the i3 Spin SIG (Special Interest Group).

This development has permitted those only wanting to install the i3wm on their machine(s) to do so without needing to install other window managers or desktop environments. Although this could have previously been accomplished by choosing a net install and picking the applicable window managers installation, this process tended to include other window managers being installed alongside i3wm. The i3 spin prevents this fromhappening, as it contains only i3wm to install. The user , therefore, does not have to waste tim, energy and effort in uninstalling other wiindow managers from their installandthe files or settings pertaining to them.

A singular benefit of the i3 spin is the continual work of the i3 Spin SIG providing a Fedora themed version of the i3wm, rather than the vanilla version. Notwithstanding the ongoing development of a Fedora themed version of the i3wm, users are alwaysable to configure their i3wm installation to their liking and even to uninstall particular default applications to the ones they prefer.

[i3 Spin Project Docs Page](https://docs.fedoraproject.org/en-US/i3/)
