# .bash_profile

export PS1="💻 \e[0;32m[\u@\h \W]\$ \e[0m "

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

# Calibre E-book Reader
export CALIBRE_USE_SYSTEM_THEME=1

# Fix for QT Overly Large Graphics & Text
export QT_QPA_PLATFORMTHEME="qt5ct"
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export QT_SCALE_FACTOR=1

# Gnome Keyring
if [ "$0" = "/usr/sbin/lightdm-session" -a "$DESKTOP_SESSION" = "i3" ]; then
    export $(gnome-keyring-daemon -s)
fi

