Qutebrowser is a Chromium-based web browser for with Vim-style key bindings and a minimal GUI. It's written in Python and Qt. Qutebrowser is a keyboard-driven browser.

Qutebrowser is free software.
